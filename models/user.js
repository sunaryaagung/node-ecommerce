const mongoose = require("mongoose");
const Joi = require("@hapi/joi");
Joi.objectId = require("joi-objectid")(Joi);
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
require("dotenv").config();

const userSchema = new mongoose.Schema({
  email: { type: String, minlength: 5, maxlength: 50, required: true },
  password: { type: String, minlength: 5, maxlength: 255, required: true },
  tasks: [{ type: mongoose.Schema.Types.ObjectId, ref: "Task" }],
  totalPoint: { type: Number, default: 0 }
});

userSchema.methods.generateToken = function() {
  const token = jwt.sign({ _id: this._id }, process.env.SECRET_KEY);
  return token;
};

userSchema.pre("save", function(next) {
  const saltRounds = 10;
  this.password = bcrypt.hashSync(this.password, saltRounds);
  next();
});

const User = new mongoose.model("User", userSchema);

function validateUser(user) {
  const schema = {
    email: Joi.string()
      .min(5)
      .max(50)
      .email()
      .required(),
    password: Joi.string()
      .min(5)
      .max(255)
      .required()
  };
  return Joi.validate(user, schema);
}

exports.User = User;
exports.validateUser = validateUser;
