const { User } = require("./user");
const mongoose = require("mongoose");
const Joi = require("@hapi/joi");
Joi.objectId = require("joi-objectid")(Joi);

const taskSchema = new mongoose.Schema({
  user: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  title: { type: String, minlength: 1, required: true },
  level: { type: Number, min: 1, max: 5, required: true },
  point: { type: Number, default: 0 },
  done: { type: Boolean, default: false }
});

taskSchema.pre("save", function(next) {
  this.point = this.level * 2;
  next();
});

taskSchema.post("save", function() {
  if ((this.done = true)) {
    this.addPoint();
  }
});

taskSchema.methods.addPoint = async function() {
  const user = await User.findById({ _id: this.user });
  user.totalPoint += this.point;
  await user.save();
};

const Task = new mongoose.model("Task", taskSchema);

//validator
function validateTask(task) {
  const schema = {
    user: Joi.objectId(),
    title: Joi.string()
      .min(1)
      .required(),
    level: Joi.number()
      .integer()
      .min(1)
      .max(5)
      .required(),
    point: Joi.number().integer(),
    done: Joi.boolean()
  };
  return Joi.validate(task, schema);
}

exports.Task = Task;
exports.validateTask = validateTask;
