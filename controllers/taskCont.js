const { User } = require("../models/user");
const { Task, validateTask } = require("../models/task");
const { successResponse, errorResponse } = require("../helpers/response");

async function get(req, res) {
  const task = await Task.find();
  res.status(200).json(successResponse("List of task", task));
}

async function add(req, res, next) {
  const { error } = validateTask(req.body);
  if (error)
    return res.status(400).json(errorResponse(error.details[0].message));

  let user = await User.findById(req.user._id);
  if (!user) return res.status(401).json({ message: "Invalid User" });

  const task = new Task({
    user: user._id,
    title: req.body.title,
    level: req.body.level
  });
  await task.save();
  user.tasks.push(task);
  await user.save();
  res.status(201).json(successResponse("Task created", task));
}

module.exports = { get, add };
