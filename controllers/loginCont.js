const { successResponse } = require("../helpers/response");
const { User, validateUser } = require("../models/user");
const bcrypt = require("bcryptjs");

async function login(req, res) {
  const { error } = validateUser(req.body);
  if (error) return res.status(400).json({ message: error.details[0].message });

  let user = await User.findOne({ email: req.body.email });
  if (!user) return res.status(401).json({ message: "Invalid Email" });

  const validPassword = await bcrypt.compare(req.body.password, user.password);
  if (!validPassword)
    return res.status(401).json({ message: "Invalid Password" });

  const token = user.generateToken();
  res.status(200).json(successResponse("Login success", token));
}

module.exports = { login };
