const { User, validateUser } = require("../models/user");
const { successResponse, errorResponse } = require("../helpers/response");

async function get(req, res) {
  user = await User.find().select("-password");
  res.status(200).json(successResponse("List of user", user));
}

async function add(req, res, next) {
  const { error } = validateUser(req.body);
  if (error) return res.status(400).json({ message: error.details[0].message });

  let user = await User.findOne({ email: req.body.email });
  if (user) return res.status(409).json({ message: "User already registered" });

  user = new User({
    email: req.body.email,
    password: req.body.password
  });

  await user.save();
  res.status(201).json(successResponse("New user saved", user));
}

module.exports = { get, add };
