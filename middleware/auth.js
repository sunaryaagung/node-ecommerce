const jwt = require("jsonwebtoken");
const { User } = require("../models/user");

module.exports = async function(req, res, next) {
  let token = req.header("Authorization");
  if (!token)
    return res.status(401).json({ message: "Access denied no token provided" });

  let tokenSplit = token.split(" ");

  try {
    let decoded = jwt.verify(tokenSplit[1], process.env.SECRET_KEY);
    req.user = decoded;

    let user = await User.findById(req.user._id);
    if (!user) return res.statsu(404).json({ message: "User not found" });

    next();
  } catch (ex) {
    res.status(401).json({ message: "Invalid Token" });
  }
};
