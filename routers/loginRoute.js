const express = require("express");
const router = express.Router();
const { login } = require("../controllers/loginCont");

router.post("/", login);

module.exports = router;
