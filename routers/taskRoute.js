const express = require("express");
const router = express.Router();
const { get, add } = require("../controllers/taskCont");
const auth = require("../middleware/auth");

router.get("/", auth, get);
router.post("/", auth, add);

module.exports = router;
