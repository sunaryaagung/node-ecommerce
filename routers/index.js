const express = require("express");
const router = express.Router();
const task = require("./taskRoute");
const user = require("./userRoute");
const login = require("./loginRoute");

router.use("/tasks", task);
router.use("/users", user);
router.use("/login", login);

module.exports = router;
